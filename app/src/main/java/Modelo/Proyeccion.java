package Modelo;

import android.database.Cursor;

import com.example.promovilexamenc2.Producto;

import java.util.List;

public interface Proyeccion {
    public Producto getProducto(String codigo);
    public List<Producto> allProductos();
    public Producto readProducto(Cursor cursor);
}
