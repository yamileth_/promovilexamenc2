package Modelo;

import com.example.promovilexamenc2.Producto;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertProducto(Producto producto);
    public long updateProducto(Producto producto);
    public void deleteProducto(int id);
}
